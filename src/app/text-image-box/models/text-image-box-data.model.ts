export interface TextImageBoxDataModel {
  name: string;
  date: Date;
  smallImgUrl: string;
  largeImageUrl: string;
  text: string;
  social: {
    likes: {
      points: number;
      count: number;
    },
    comments: {
      points: number;
      count: number;
    },
    shares: {
      points: number;
      count: number;
    }
  };
}
